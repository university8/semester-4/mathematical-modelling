import math
import statistics
import scipy.integrate
import random

exp_value = 0.5
log_a = 0
log_b = 1.5
cauchy_a = -1
cauchy_b = 3
laplace_a = 2
m1 = -4
s2_1 = 4
m2 = 5
s2_2 = 9


def kolmogorov(arr, distr, length):
    arr.sort()
    supr = 0
    for i in range(length):
        el = distr(arr[i])
        if supr < math.fabs(float(i + 1) / length - el):
            supr = math.fabs(float(i + 1) / length - el)

    return math.sqrt(length) * supr


def pearson(arr, length, m, distr):
    arr.sort()
    minVal = min(arr)
    cellSize = (max(arr) - minVal) / m
    j = 0
    hi = 0

    for i in range(1, m):
        count = 0
        bound = minVal + cellSize * i
        while arr[j] < bound and j < length:
            j += 1
            count += 1
            # print(count)

        prob = distr(minVal + cellSize * i) - distr(minVal + cellSize * (i - 1))
        hi += (count - length * prob) * (count - length * prob) / length * prob

    return hi


def func1(x):
    return math.exp(-math.pow(x - m1, 2) / 2 / s2_1)


def func2(x):
    return math.exp(-math.pow(x - m2, 2) / 2 / s2_2)


def firstNormalDistr(val):
    integr = scipy.integrate.quad(func1, -1000, val)
    # print(integr[0])
    return integr[0] / math.sqrt(2 * math.pi * s2_1)


def secondNormalDistr(val):
    integr = scipy.integrate.quad(func2, -1000, val)
    return integr[0] / math.sqrt(2 * math.pi * s2_2)


def laplaceDistribution(val):
    if val < 0:
        return 0.5 * math.exp(laplace_a * val)
    return 1 - 0.5 * math.exp(laplace_a * val)


def expDistribution(val):
    return 1 - math.exp(-exp_value * val)


def logisticDistribution(val):
    return 1 / (1 + math.exp(-(val - log_a) / log_b))


def cauchyDistribution(val):
    return 0.5 + 1 / math.pi * math.atan((val - cauchy_a) / cauchy_b)


def laplaceModelling(l, n):
    seq = []
    for i in range(n):
        y = random.random()
        if y < 0.5:
            seq.append(1 / l * math.log(2 * y))
        else:
            seq.append(-1 / l * math.log(2 * (1 - y)))

    return seq


def cauchyModelling(m, c, n):
    seq = []
    for i in range(n):
        seq.append(m + c * math.tan(math.pi * (random.random() - 0.5)))

    return seq


def logisticModelling(u, k, n):
    seq = []
    for i in range(n):
        y = random.random()
        seq.append(u + k * math.log(y / (1 - y)))

    return seq


def expModelling(l, n):
    seq = []
    for i in range(n):
        y = random.random()
        seq.append(-math.log(1 - y) / l)

    return seq


def normalModelling(m, s2, length):
    seq = [0] * length
    for i in range(length):
        sm = 0
        for j in range(12):
            sm += random.random()

        seq[i] = m + (sm - 6) * math.sqrt(s2)

    return seq


def main():
    n = 1000
    m = 10

    logistic_arr = logisticModelling(log_a, log_b, n)
    exp_arr = expModelling(exp_value, n)
    laplace_arr = laplaceModelling(laplace_a, n)
    cauchy_arr = cauchyModelling(cauchy_a, cauchy_b, n)
    firstNorm = normalModelling(m1, s2_1, n)
    secondNorm = normalModelling(m2, s2_2, n)

    print('Нормальное распределение 1: ')
    print('МО ', statistics.mean(firstNorm))
    print('Теоритическое МО ', m1)
    print('Дисперсия ', statistics.variance(firstNorm))
    print('Теоритическая дисперсия ', s2_1)
    print('Пирсон: ', pearson(firstNorm, n, m, firstNormalDistr))
    print('Колмогоров: ', kolmogorov(firstNorm, firstNormalDistr, n))
    print('\n')

    print('Нормальное распределение 2: ')
    print('МО ', statistics.mean(secondNorm))
    print('Теоритическое МО ', m2)
    print('Дисперсия ', statistics.variance(secondNorm))
    print('Теоритическая дисперсия ', s2_2)
    print('Пирсон: ', pearson(secondNorm, n, m, secondNormalDistr))
    print('Колмогоров: ', kolmogorov(secondNorm, secondNormalDistr, n))
    print('\n')

    print('Экспоненциальное распределение: ')
    print('МО ', statistics.mean(exp_arr))
    print('Теоритическое МО ', 1 / exp_value)
    print('Дисперсия ', statistics.variance(exp_arr))
    print('Теоритическая дисперсия ', 1 / exp_value ** 2)
    print('Пирсон: ', pearson(exp_arr, n, m, expDistribution))
    print('Колмогоров: ', kolmogorov(exp_arr, expDistribution, n))
    print('\n')

    print('Логистическое распределение : ')
    print('МО ', statistics.mean(logistic_arr))
    print('Теоритическое МО ', log_a)
    print('Дисперсия ', statistics.variance(logistic_arr))
    print('Теоритическая дисперсия ', math.pi * math.pi * log_b * log_b / 3)
    print('Пирсон: ', pearson(logistic_arr, n, m, logisticDistribution))
    print('Колмогоров: ', kolmogorov(logistic_arr, logisticDistribution, n))
    print('\n')

    print('Распределение Лапласа: ')
    print('МО ', statistics.mean(laplace_arr))
    print('Теоритическое МО ', 0)
    print('Дисперсия ', statistics.variance(laplace_arr))
    print('Теоритическая дисперсия ', 2 / laplace_a ** 2)
    print('Пирсон: ', pearson(laplace_arr, n, m, laplaceDistribution))
    print('Колмогоров: ', kolmogorov(laplace_arr, laplaceDistribution, n))
    print('\n')

    print('Коши: ')
    print('МО ', statistics.mean(cauchy_arr))
    print('Теоритическое МО  -')
    print('Дисперсия ', statistics.variance(cauchy_arr))
    print('Теоритическая дисперсия  ∞')
    print('Пирсон: ', pearson(cauchy_arr, n, m, cauchyDistribution))
    print('Колмогоров: ', kolmogorov(cauchy_arr, cauchyDistribution, n))
    print('\n')


if __name__ == "__main__":
    main()
