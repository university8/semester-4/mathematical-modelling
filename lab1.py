import math


def kolmogorov(arr, n):
    arr.sort()
    supr = 0
    for i in range(n):
        if supr < math.fabs(float(i + 1) / n - arr[i]):
            supr = math.fabs(float(i + 1) / n - arr[i])

    if math.sqrt(n) * supr < 1.36:
        return True
    else:
        return False
    # return supr


def pirson(arr, n, m):
    arr.sort()
    j = 0
    hi = 0

    for i in range(1, m):
        count = 0

        while arr[j] < (float(i) / m) and j < n:
            j += 1
            count += 1
            print(count)


        hi += (count - (n / m)) * (count - (n / m)) / (n / m)

    if hi <3:
        return True
    else:
        return False


def maklarenMarsolyi(firstArr, secondArr, k):
    n = len(firstArr)
    table = firstArr[0:k]
    sequence = []
    for i in range(n):
        sequence.append(table[int(secondArr[i] * k)])
        table[int(secondArr[i] * k)] = firstArr[(i + k) % n]

    return sequence


def multiplicativelyСongruent(m, betta, n):
    x = []
    b = []
    x.append(betta)
    b.append(x[0] / m)
    for i in range(1, n):
        x.append(betta * x[i - 1] % m)
        b.append(x[i] / m)

    return b


m = 2147483648
betta = 131075
n = 1000
k = 128
firstSeq = multiplicativelyСongruent(m, betta, n)
par2 = multiplicativelyСongruent(m, betta * 4 + 1, n)
secondSeq = maklarenMarsolyi(firstSeq, par2, k)
print('Критерий Колмогорова для первой последовательности: ')
print(kolmogorov(firstSeq, n))
print('Критерий Пирсона для первой последовательности: ')
print(pirson(firstSeq, n, 10))
print('Критерий Колмогорова для второй последовательности: ')
print(kolmogorov(secondSeq, n))
print('Критерий Пирсона для второй последовательности: ')
print(pirson(secondSeq, n, 10))
