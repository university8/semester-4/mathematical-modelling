import math
import random
import itertools
import statistics


def C(n, k):
    return len(list(itertools.combinations(range(0, n), k)))


def binom(n, m, p):
    res = []
    for i in range(n):
        x = 0
        for j in range(m):
            item = p - random.random()
            if item > 0:
                x += 1
        res.append(x)
    return res


def negativeBinom(n, r, p):
    seq = [0] * n
    x = 0
    rm = r
    for i in range(n):
        while rm != 0:
            if random.random() > p:
                x += 1
            else:
                rm -= 1

        seq[i] = x
        rm = r
        x = 0

    return seq


def poisson(par, n):
    pr = 0.0
    r = 0.0
    l = math.exp(-par)
    seq = []
    for i in range(n):
        r = random.random()
        pr = r
        k = 0
        while (pr > l):
            r = random.random()
            pr *= r
            k += 1

        seq.append(k)

    return seq


def pirsonForPoisson(seq, par):
    maxVal = max(seq)
    probabilities = []
    pr = math.exp(-par)
    probabilities.append(pr)
    lenOfSeq = len(seq)
    for i in range(1, maxVal + 1):
        pr *= par / i
        probabilities.append(pr)

    amountOfValues = [0] * maxVal
    for i in range(maxVal + 1):
        amountOfValues[seq[i]] += 1

    hi = 0
    for i in range(maxVal):
        hi += (amountOfValues[i] / lenOfSeq -
               probabilities[i]) * (amountOfValues[i] / lenOfSeq -
                                    probabilities[i]) / probabilities[i]

    return hi


def pirsonForBinom(seq, m, p):
    probabilities = []
    amountOfValues = [0] * (m + 1)
    lenOfSeq = len(seq)
    for i in range(lenOfSeq):
        amountOfValues[seq[i]] += 1

    for i in range(m + 1):
        prob1 = C(m, i)
        prob2 = (p ** i)
        prob3 = ((1 - p) ** (m - i))
        prob = prob1 * prob2 * prob3
        probabilities.append(prob)

    hi = 0
    for i in range(m):
        hi += (amountOfValues[i] / lenOfSeq -
               probabilities[i]) * (amountOfValues[i] / lenOfSeq -
                                    probabilities[i]) / probabilities[i]

    return hi


def pirsonForNegBinom(seq, r, p):
    maxVal = max(seq)
    probabilities = []
    amountOfValues = [0] * maxVal
    lenOfSeq = len(seq)
    for i in range(lenOfSeq):
        amountOfValues[seq[i] - 1] += 1

    for i in range(maxVal + 1):
        prob1 = C(i + r - 1, i)
        prob2 = (p ** r)
        prob3 = ((1 - p) ** i)
        prob = prob1 * prob2 * prob3
        probabilities.append(prob)

    hi = 0
    for i in range(r):
        hi += (amountOfValues[i] / lenOfSeq -
               probabilities[i]) * (amountOfValues[i] / lenOfSeq -
                                    probabilities[i]) / probabilities[i]

    return hi


def main():
    bin_m = 6
    bin_p = 0.3333333
    negBin_r = 4
    negBin_p = 0.2
    bin2_m = 5
    bin2_p = 0.75
    poisson_l = 2
    n = 1000

    firstBin = binom(n, bin_m, bin_p)
    secondBin = binom(n, bin2_m, bin2_p)
    negativeBin = negativeBinom(n, negBin_r, negBin_p)
    poissonArr = poisson(poisson_l, n)

    print("Mean")
    print("Binom: {} - {}".format(bin_m * bin_p, statistics.mean(firstBin)))
    print("Second binom: {} - {}".format(bin2_m * bin2_p, statistics.mean(secondBin)))
    print("Negative binom: {} - {}".format((negBin_r * (1 - negBin_p) / negBin_p), statistics.mean(negativeBin)))
    print("Poisson: {} - {}".format(poisson_l, statistics.mean(poissonArr)))
    print("Variance")
    print("Binom: {} - {}".format(bin_m * bin_p * (1 - bin_p), statistics.variance(firstBin)))
    print("Second binom:{} - {}".format(bin2_m * bin2_p * (1 - bin2_p), statistics.variance(secondBin)))
    print("Negative binom:{} - {}".format((negBin_r * (1 - negBin_p) / negBin_p ** 2), statistics.variance(negativeBin)))
    print("Poisson{} - {}".format(poisson_l, statistics.variance(poissonArr)))

    print('Pearson:')
    print(pirsonForBinom(firstBin, bin_m, bin_p))
    print(pirsonForBinom(secondBin, bin2_m, bin2_p))
    print(pirsonForNegBinom(negativeBin, negBin_r, negBin_p))
    print(pirsonForPoisson(poissonArr, poisson_l))


if __name__ == "__main__":
    main()
